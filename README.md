# Docker environments

## Requirements
> Gradle 7.3
> 
> Docker 20.10
> 
> Docker Compose 1.29

## DEV
> chmod +x deploy-dev.sh
> 
> ./deploy-dev.sh


## QA
> chmod +x deploy-qa.sh
>
> ./deploy-qa.sh
